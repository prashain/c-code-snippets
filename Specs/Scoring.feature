Feature: Scoring
	In order to keep track of my score
	As a bowler
	I want to use automated scoring system with some feedback
	
@mytag
Scenario: Bowling a strike
	Given I am on the first frame
	When I bowl a strike
	Then I should see an "X" and a message that says "Good Job"
	
	
Scenario: Bowling a gutter ball as first throw
	Given I am on the first frame
	When I bowl a gutter ball
	Then I should see an "0" and a message that says "You'll do better next time"
	
	
Scenario: Bowling two gutter ball in row
	Given I have bowled a gutter bowl already
	When I bowl another gutter ball
	Then I should see an "0" and a message that says "You need lessons"
