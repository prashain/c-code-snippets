using System;

using GenericStack;

using TechTalk.SpecFlow;
using NUnit.Framework;

namespace Specs
{
	[Binding]
	public class GenericStackSteps
	{
        
		Stack<string> myStack;
		string lastPushedvalue;
		Boolean isStackEmpty;
		
		[Given(@"I have a stack")]
		public void GivenIHaveAStack ()
		{
			myStack = new Stack<string> ();
		}

		[When(@"I push an element")]
		public void WhenIPushAnElement ()
		{

			myStack.Push ("tmp");

		}

		[Then(@"Stack should have size (.*)")]
		public void ThenStackShouldHaveSize (int p0)
		{
			
			Assert.AreEqual (p0, myStack.Size);
		}
				
		[When(@"I pop an element")]
		public void WhenIPopAnElement ()
		{
			myStack.Push ("tmp");
			lastPushedvalue = myStack.Pop ();
		}
		
		[Then(@"I should get last pushed element")]
		public void ThenIShouldGetLastPushedElement ()
		{
			Assert.AreEqual (lastPushedvalue, "tmp");
		}
		[Then(@"stack size should be decremented")]
		public void ThenStackSizeShouldBeDecremented ()
		{
			Assert.AreEqual (0, myStack.SizeOfStack ());
		}
		
		
		[Given(@"I have a stack with no elements")]
		public void GivenIHaveAStackWithNoElements ()
		{

			myStack = new Stack<string> ();

		}
		[When(@"I pop an element on empty stack")]
		public void WhenIPopAnElementOnEmptyStack ()
		{
			lastPushedvalue = myStack.Pop ();
		}
		
		[Then(@"I should get null")]
		public void ThenIShouldGetNull ()
		{
			Assert.IsNull (lastPushedvalue);
		}
		
		
		
		[Given(@"I have a stack with some elements")]
		public void GivenIHaveAStackWithSomeElements ()
		{
			myStack = new Stack<string> ().Push ("foo").Push ("bar").Push ("baz");
		}
        
		[When(@"I peek for an element")]
		public void WhenIPeekForAnElement ()
		{
			lastPushedvalue = myStack.Peek ();
		}
        
		[Then(@"I should get last pushed element in stack")]
		public void ThenIShouldGetLastPushedElementInStack ()
		{
			Assert.AreEqual (lastPushedvalue, "baz");
		}
		[Then(@"stack size should not be modified")]
		public void ThenStackSizeShouldNotBeModified ()
		{
			Assert.AreEqual (myStack.SizeOfStack (), 3);
		}
		
		[Given(@"I have a stack with no element")]
		public void GivenIHaveAStackWithNoElement ()
		{
			myStack = new Stack<string> ();
		}

		[When(@"I query for IsEmpty")]
		public void WhenIQueryForIsEmpty ()
		{
			isStackEmpty = myStack.IsEmpty ();
		}

		[Then(@"I should true")]
		public void ThenIShouldTrue ()
		{
			Assert.AreEqual (isStackEmpty, true);
		}	
	}
}
		
