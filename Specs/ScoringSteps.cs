using System;

using TechTalk.SpecFlow;

using NUnit.Framework;
using SuperBlowlingScorer;


namespace Specs
{
	[Binding]
    public class ScoringSteps
	{
		
		SuperScorer _scorer;
		
	[Given(@"I am on the first frame")]
        public void GivenIAmOnTheStrikeFrame()
        {
           _scorer = new SuperScorer();
			Assert.AreEqual(1,_scorer.Frame);
   
		}
	 [When(@"I bowl a strike")]
        public void WhenIBowlAStrike()
        {
            _scorer.ScoreFirstBall(10);
        }	
		
       [Then(@"I should see an ""(.*)"" and a message that says ""(.*)""")]
        public void ThenIShouldSeeAnAndAMessageThatSays(string p0, string p1)
        {
            Assert.AreEqual(p1,_scorer.BowlerMessage);
			Assert.AreEqual(p0,_scorer.Score);
        }
		

        [When(@"I bowl a gutter ball")]
        public void WhenIBowlAGutterBall()
        {
            _scorer.ScoreFirstBall(0);
        }	
		
		
        [Given(@"I have bowled a gutter bowl already")]
        public void GivenIHaveBowledAGutterBowlAlready()
        {
 			_scorer = new SuperScorer();
			_scorer.ScoreFirstBall(0);
        }
		
        [When(@"I bowl another gutter ball")]
        public void WhenIBowlAnotherGutterBall()
        {
            _scorer.ScoreSecondBall(0);
            
        }
		
	}
}
		
