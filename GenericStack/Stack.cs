using System;

namespace GenericStack
{
	public class Stack<T>
	{
		// storage area for stack.
		private T[] _storage;
		public int Size {
			get;
			set;
		}
		
		public Stack ()
		{
			 _storage = new T[4];
			Size = 0;
		}
		
		
		// while pushing ensureCapacity i.e increase default size 
		public Stack<T> Push(T elementToPush){
			if(null != elementToPush){
					_storage[Size] = elementToPush;
					Size++;
				return this;
			}else{
				throw new ArgumentException("Cannot pass null as argument");
			}

		}
		public int SizeOfStack(){
			return Size;
		}
		
		/***
		 * 
		 * 
		 * Pops an element from stack.
		 * 
		 */
		public T Pop(){
			if(Size == 0)
					return  default(T);
			else{
				T returnValue = _storage[(Size -1)];
				Size--;
				return returnValue;

			}
					
		}
		
		public T Peek(){
			T returnValue;
			if(Size == 0)
				returnValue = default(T);
			else 
				returnValue = _storage[(Size -1)];
			
			return returnValue;
				
		}
		
		public Boolean IsEmpty(){
			return (Size == 0);
		}
		}
}


